package com.example.banque.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.banque.model.User;
import com.example.banque.repository.UserRepository;

@Controller
@RequestMapping(path="/user") 
public class UserController {

    @Autowired
    UserRepository userRepository;
    
	@GetMapping("/add")  
	public String addUser( Model model) {
	model.addAttribute("user", new User());
	return "users/user";
	}
    @PostMapping("/add") 
	public String addUser(@Valid User user ,BindingResult bindingResult)
    {
	  if (bindingResult.hasErrors()) {
			return "users/user"; 
		}	  	
	  userRepository.save(user);
	  return "redirect:/user/all";
	 
	}
	@GetMapping("/all")
	public String Users( Model model) {
	model.addAttribute("users", userRepository.findAll());
	return "users/all";
	}

}
