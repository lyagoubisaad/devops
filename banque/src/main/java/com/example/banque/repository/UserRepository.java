package com.example.banque.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.banque.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

}